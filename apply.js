const fs = require('fs');
const path = require('path');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers')
const {getHashtags, getOriginalNonDmStatuses} = require("./utils");

const argv = yargs(hideBin(process.argv))
  .option('accountId', {
    description: 'Account ID on the new instance',
    type: 'string',
    demand: true
  })
  .option('oldInstance', {
    description: 'Hostname of the old instance, e.g. old-mastodon.social',
    type: 'string',
    demand: true
  })
  .option('newInstance', {
    description: 'Hostname of the new instance, e.g. new-mastodon.social',
    type: 'string',
    demand: true
  })
  .option('oldUsername', {
    description: 'Username on the old instance',
    type: 'string',
    demand: true
  })
  .option('newUsername', {
    description: 'Username on the new instance',
    type: 'string',
    demand: true
  })
  .option('exportDir', {
    description: 'Directory to the export Mastodon archive',
    type: 'string',
    demand: true
  })
  .demandOption(['accountId', 'oldInstance', 'newInstance', 'oldUsername', 'newUsername', 'exportDir'])
  .argv

const outboxJson = path.join(argv.exportDir, 'outbox.json')
const data = fs.readFileSync(outboxJson, 'utf-8')
const orderedItems = JSON.parse(data)['orderedItems']
const items = getOriginalNonDmStatuses(orderedItems, argv);

console.log('BEGIN;');

// write SQLs for statues
items.forEach(item => {
  const {id, uri, text, date, visibility, cw} = item
  if (!!cw) {
    const query = `INSERT INTO statuses (id, uri, text, created_at, updated_at, sensitive, visibility, spoiler_text, local, account_id, application_id) VALUES ('${id}', '${uri}', '${text}', '${date}', '${date}', 'f', '${visibility}', '${cw}', 't', '${argv.accountId}', '1');`;
    console.log(query);
  } else if (text) {
    const query = `INSERT INTO statuses (id, uri, text, created_at, updated_at, sensitive, visibility, local, account_id, application_id) VALUES ('${id}', '${uri}', '${text}', '${date}', '${date}', 'f', '${visibility}', 't', '${argv.accountId}', '1');`;
    console.log(query);
  }
})

const {unique, all} = getHashtags(items)

// write SQLs for tags
unique.forEach(hashtag => {
  const {name, created} = hashtag
  const query = `INSERT INTO tags (name, created_at, updated_at) VALUES ('${name}', '${created}', '${created}') ON CONFLICT DO NOTHING;`;
  console.log(query)
})

// write SQLs for tag <-> status relationships
all.forEach(hashtag => {
  const {name, status_id} = hashtag
  const query = `INSERT INTO statuses_tags (status_id, tag_id) VALUES ('${status_id}', (select id from tags where name ='${name}'));`
  console.log(query)
})

console.log('COMMIT;');
