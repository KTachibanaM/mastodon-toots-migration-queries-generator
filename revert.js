const fs = require('fs');
const path = require('path');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers')
const {getHashtags, getOriginalNonDmStatuses} = require("./utils");

const argv = yargs(hideBin(process.argv))
  .option('accountId', {
      description: 'Account ID on the new instance',
      type: 'string',
      demand: true
  })
  .option('oldInstance', {
      description: 'Hostname of the old instance, e.g. old-mastodon.social',
      type: 'string',
      demand: true
  })
  .option('oldUsername', {
      description: 'Username on the old instance',
      type: 'string',
      demand: true
  })
  .option('exportDir', {
      description: 'Directory to the export Mastodon archive',
      type: 'string',
      demand: true
  })
  .demandOption(['accountId', 'oldInstance', 'oldUsername', 'exportDir'])
  .argv

const outboxJson = path.join(argv.exportDir, 'outbox.json')
const data = fs.readFileSync(outboxJson, 'utf-8')
const orderedItems = JSON.parse(data)['orderedItems']
const items = getOriginalNonDmStatuses(orderedItems, argv);

console.log('BEGIN;');

// write SQLs for statues
items.forEach(item => {
    const {id, cw, text} = item
    if (!!cw || text) {
        const query = `DELETE FROM statuses WHERE id = ${id};`;
        console.log(query);
    }
})

const {unique} = getHashtags(items)

// write SQLs for tags
unique.forEach(hashtag => {
    const {name} = hashtag
    const query = `DELETE FROM tags WHERE name = '${name}';`;
    console.log(query)
})

// write SQLs for tags
console.log('COMMIT;');
