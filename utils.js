const htmlToText = require('html-to-text')

const isVisibilityFollowersOnly = (item) => {
  return item['to'].length > 0
    && item['to'][0].endsWith('/followers')
    && item['cc'][0] !== 'https://www.w3.org/ns/activitystreams#Public';
}

const isVisibilityUnlisted = (item) => {
  return item['to'].length > 0
    && item['to'][0].endsWith('/followers')
    && item['cc'][0] === 'https://www.w3.org/ns/activitystreams#Public';
}

const isVisibilityPublic = (item) => {
  return item['to'][0] === 'https://www.w3.org/ns/activitystreams#Public'
    && item['cc'].length > 0
    && item['cc'][0].endsWith('/followers');
}

const getStatusVisibility = (item) => {
  if (isVisibilityPublic(item)) {
    return 0;
  }
  if (isVisibilityUnlisted(item)) {
    return 1;
  }
  if (isVisibilityFollowersOnly(item)) {
    return 2;
  }
  // Mastodon does not have full support of limited visibility (circle/aspect) as of v3.2.0
  // Every other visibility types are treated as direct messages here
  return 3;
}

const getDate = (item) => {
  return item['published']
    .replace('T', ' ')
    .replace('Z', '.000');
}

const getHashtags = (items) => {
  const tagsByType = {};
  items.forEach(item => {
    item['tags'].forEach(tag => {
      if (!tagsByType[tag['type']]) {
        tagsByType[tag['type']] = [];
      }
      tag['id'] = item['id'];
      tag['created'] = item['date'];
      tagsByType[tag['type']].push(tag)
    });
  });
  const hashtagNames = new Set();
  const uniqueHashtags = [];
  const allHashtags = []
  tagsByType['Hashtag'].forEach(tag => {
    const tagObj = {
      name: tag['name'].replace('#', ''),
      created: tag['created'],
      status_id: tag['id']
    }
    if (!hashtagNames.has(tag['name'])) {
      hashtagNames.add(tag['name']);
      uniqueHashtags.push(tagObj)
    }
    allHashtags.push(tagObj)
  });
  return {unique: uniqueHashtags, all: allHashtags}
}

const getOriginalNonDmStatuses = (items, config) => {
  return items
    .filter(item => {
      return item['object']['inReplyTo'] === null
        || (item['object']['inReplyTo']
          && item['object']['inReplyTo']
            .startsWith(`https://${config.oldInstance}/users/${config.oldUsername}`)
        );
    })
    .map(item => {
      const id_splits = item['id'].split('/')
      const id = id_splits[id_splits.length - 2];
      const uri = item['id']
        .replace(config.oldInstance, config.newInstance)
        .replace(config.oldUsername, config.newUsername)
        .replace('/activity', '');
      const date = getDate(item);
      const content = item['object']['content']
      let text = htmlToText.htmlToText(content, {
        tags: {
          'a': {
            options: {
              ignoreHref: true,
            }
          }
        }
      })
      text = text.replace("'", "''")
      const cw = item['object']['summary'];
      const visibility = getStatusVisibility(item);
      const tags = item['object']['tag']
      return {
        id, uri, text, date, visibility, cw, tags
      }
    })
    .filter(item => item['visibility'] !== 3)
}

exports.getHashtags = getHashtags
exports.getOriginalNonDmStatuses = getOriginalNonDmStatuses
